<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carpeta extends Model
{
    protected $table = "carpeta";
    protected $primaryKey = "idCarpeta";
    protected $fillable = [
        "idCarpeta",
        "idnombreDocumento",
        "direccionDocumento",
        "cedulaEst"
    ];
    public $timestamps = false;
    /* public function estadodos(){ //carpeta pertenece a uno o varios estados
        return $this->hasMany('App\Estado','idCarpeta');
    } */
    public function nombre(){ 
        return $this->belongsTo('App\nombreDocumento','idnombreDocumento');
    }
    public function estados()
    {
        return $this->belongsToMany('App\Estado', 'Estado_has_carpeta', 'idCarpeta', 'idEstado')->withPivot('observacion', 'fecha');
    }
}
