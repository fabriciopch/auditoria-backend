<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado_has_carpeta extends Model
{
    protected $table = "estado_has_carpeta";
    protected $primaryKey = ["idCarpeta","idEstado"];
    protected $fillable = [        
        "observacion"
    ];
    const fecha = "fecha";
    protected $date=["fecha"];
    
    public $timestamps = false;
}
