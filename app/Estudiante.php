<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estudiante extends Model
{
    protected $table = "estudiante";
    protected $primaryKey = "idEstudiante";
    protected $fillable = [
        "idEstudiante",
        "cedula",
        "primerNombre",
        "segundoNombre",
        "primerApellido",
        "segundoApellido",
        "direccion"
    ];
    public $timestamps = false;

    //un estudiante tiene una solo carpeta
    
    /* public function carpeta(){ 

        return $this->hasOne('App\Carpeta','foreign_key','idEstudiante');

    } */
}
