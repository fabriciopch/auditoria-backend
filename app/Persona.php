<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $table = "persona";
    protected $primaryKey = "Id";
    protected $fillable = [
        "Id",
        "nombre_per"
    ];
    public $timestamps = false;
}
