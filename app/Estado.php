<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $table = "estado";
    protected $primaryKey = "idEstado";
    protected $fillable = [
        "idEstado",
        "valor",
        "observacion",
        "idCarpeta"
    ];
    public $timestamps = false;
    /* public function carpeta(){ //el estado pertenece a una solo carpeta
        return $this->belongsTo('App\Carpeta','idCarpeta');
    } */
    public function carpetas()
    {
        return $this->belongsToMany('App\Carpeta', 'Estado_has_carpeta', 'idEstado', 'idCarpeta')->withPivot('observacion', 'fecha');
    }
}
