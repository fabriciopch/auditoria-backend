<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class nombreDocumento extends Model
{
    protected $table = "nombre_Documento";
    protected $primaryKey = "idnombreDocumento";
    protected $fillable = [
        
        "idnombreDocumento",
        "nombre"
    ];
    public $timestamps = false;

    public function documentos(){ 

        return $this->hasOne('App\Carpeta','foreign_key','idnombreDocumento');

    }

}
