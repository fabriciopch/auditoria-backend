<?php

namespace App\Http\Controllers;

use App\nombreDocumento;
use Illuminate\Http\Request;

class NombreDocumentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nombreDocumento = nombreDocumento::all();
        return response()->json($nombreDocumento);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\nombreDocumento  $nombreDocumento
     * @return \Illuminate\Http\Response
     */
    public function show(nombreDocumento $nombreDocumento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\nombreDocumento  $nombreDocumento
     * @return \Illuminate\Http\Response
     */
    public function edit(nombreDocumento $nombreDocumento)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\nombreDocumento  $nombreDocumento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, nombreDocumento $nombreDocumento)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\nombreDocumento  $nombreDocumento
     * @return \Illuminate\Http\Response
     */
    public function destroy(nombreDocumento $nombreDocumento)
    {
        //
    }
}
