<?php

namespace App\Http\Controllers;

use App\Estado_has_carpeta;
use Illuminate\Http\Request;

class EstadoHasCarpetaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Estado_has_carpeta  $estado_has_carpeta
     * @return \Illuminate\Http\Response
     */
    public function show(Estado_has_carpeta $estado_has_carpeta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Estado_has_carpeta  $estado_has_carpeta
     * @return \Illuminate\Http\Response
     */
    public function edit(Estado_has_carpeta $estado_has_carpeta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Estado_has_carpeta  $estado_has_carpeta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Estado_has_carpeta $estado_has_carpeta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Estado_has_carpeta  $estado_has_carpeta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Estado_has_carpeta $estado_has_carpeta)
    {
        //
    }
}
