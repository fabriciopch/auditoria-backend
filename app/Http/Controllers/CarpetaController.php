<?php

namespace App\Http\Controllers;

use App\Carpeta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;


class CarpetaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $carpeta=Carpeta::all()->load('nombre','estados');
        return response()->json($carpeta);

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        
        $carpeta = Carpeta::create($request->all());  
        if($request->hasFile('direccionDocumento')){
            $file = $request->file('direccionDocumento');
            $name = time().$file->getClientOriginalName();
            $path = '/documentos/';
            $carpeta->direccionDocumento = $request->file('direccionDocumento')->storeAs($path,$name); 
        }
        
        /* $carpeta->direccionDocumento = $request->file('direccionDocumento')->store('/documentos/');   */  
               
        $carpeta->save();       
        return response()->json($carpeta,201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Carpeta  $carpeta
     * @return \Illuminate\Http\Response
     */
    public function show(Carpeta $carpeta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Carpeta  $carpeta
     * @return \Illuminate\Http\Response
     */
    public function edit(Carpeta $carpeta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Carpeta  $carpeta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {       
        $carpeta = Carpeta::findOrFail($id); 

        /* disk('local') ===> accedera a la ruta app/documentos  configurado en filesystems.php*/
        $exist = \Storage::disk('local')->exists($carpeta->direccionDocumento);
        if (isset($carpeta->direccionDocumento) && $exist) {
            $delete = \Storage::disk('local')->delete($carpeta->direccionDocumento);            
            $file = $request->file('direccionDocumento');
            $name = time().$file->getClientOriginalName();
            $path = '/documentos/';
            $carpeta->direccionDocumento = $request->file('direccionDocumento')->storeAs($path,$name); 
        }           
       /*  $carpeta->update($request->all());  */ 
        $carpeta->save();   
        return response()->json($carpeta,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Carpeta  $carpeta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Carpeta $carpeta)
    {
        $carpeta->delete();
        return response()->json(null, 204);
    }
}
