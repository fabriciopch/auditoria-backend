<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

 Route::middleware('auth:api','cors')->get('/user', function (Request $request) {
    return $request->user();
}); 

Route::resource('personas','PersonaController'); 
/* Route::resource('estudiantes','EstudianteController');
Route::resource('carpetas','CarpetaController');
 */
Route::resource('estudiantes','EstudianteController');
Route::resource('carpetas','CarpetaController');
Route::resource('es','estudianteC');
Route::get('nomDoc','NombreDocumentoController@index');

Route::group([
    'prefix' => 'auth',
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});
  
/* Route::group(['middleware']=>['auth:api','cors'],function(){
    return $request->user();
    

}); */